import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ProfileService } from 'src/app/profile/services/profile.service';
import { CurrentRegistrationService } from '../../services/currentRegistrationService.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AngularDataContext, DATA_CONTEXT_CONFIG } from '@themost/angular';
import { ConfigurationService } from 'src/app/shared/services/configuration.service';
import { APP_INITIALIZER } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { ModalModule, TooltipModule } from 'ngx-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgPipesModule } from 'ngx-pipes';

import { RegistrationCheckoutComponent } from './registration-checkout.component';

xdescribe('RegistrationCheckoutComponent', () => {
  let component: RegistrationCheckoutComponent;
  let fixture: ComponentFixture<RegistrationCheckoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrationCheckoutComponent ],
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        ModalModule.forRoot(),
        ToastrModule.forRoot(),
        SharedModule,
        TooltipModule.forRoot(),
        NgPipesModule
       ],
      providers: [
        ProfileService,
        CurrentRegistrationService,
        AngularDataContext,
        {
          provide: DATA_CONTEXT_CONFIG, useValue: {
              base: '/',
              options: {
                  useMediaTypeExtensions: false
              }
          }
        },
        ConfigurationService,
        {
            provide: APP_INITIALIZER,
            useFactory: (configurationService: ConfigurationService) =>
                () => {
                    configurationService.config = {
                        settings: {
                            localization: {
                                cultures: [ 'en', 'el' ],
                                default: 'en'
                            }
                        }
                    };
                    return Promise.resolve();
                },
            deps: [ ConfigurationService ],
            multi: true
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationCheckoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
