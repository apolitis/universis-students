import { Component, OnInit } from '@angular/core';
import { CurrentRegistrationService } from '../../services/currentRegistrationService.service';
import { Router } from '@angular/router';
import {BsModalRef} from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-registration-specialty',
  templateUrl: './registration-specialty.component.html',
  styleUrls: ['./registration-specialty.component.scss']
})
export class RegistrationSpecialtyComponent implements OnInit {

  public specialties;
  public selectedSpecialty;
  public currentSpecialty;
  public isLoading = true;
  public disabled = false;
  public lastError;
  constructor(public bsModalRef: BsModalRef,
              private currentReg: CurrentRegistrationService,
              private _router: Router) { }

  ngOnInit() {
    this.currentReg.getSpecialties().then(res => {
      this.specialties = res.specialties;
      this.currentReg.getCurrentSpecialty().then(currentSpec => {
        this.currentSpecialty = currentSpec;
        this.isLoading = false;
      });
    });
  }

  selectSpecialty() {
    this.lastError = null;
    const studyProgramSpecialty = this.specialties.find( x => {
      return x.id === this.selectedSpecialty;
    });
    this.disabled = true;
    this.currentReg.setSpecialty(studyProgramSpecialty).then(() => {
      this.bsModalRef.hide();
      this.disabled = false;
      return this._router.navigateByUrl('/registrations');
    }).catch( err => {
      // log error to console
      this.disabled = false;
      console.log(err);
      this.lastError = err;
    });
  }
}
