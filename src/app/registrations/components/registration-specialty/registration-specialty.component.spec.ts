import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ConfigurationService } from 'src/app/shared/services/configuration.service';
import { BsModalRef } from 'ngx-bootstrap';
import { APP_INITIALIZER } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CurrentRegistrationService } from '../../services/currentRegistrationService.service';
import { AngularDataContext, DATA_CONTEXT_CONFIG } from '@themost/angular';
import { ProfileService } from '../../../profile/services/profile.service';
import { RouterTestingModule } from '@angular/router/testing';

import { RegistrationSpecialtyComponent } from './registration-specialty.component';

xdescribe('RegistrationSpecialtyComponent', () => {
  let component: RegistrationSpecialtyComponent;
  let fixture: ComponentFixture<RegistrationSpecialtyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrationSpecialtyComponent ],
      imports: [ FormsModule, TranslateModule.forRoot(), HttpClientTestingModule, RouterTestingModule ],
      providers: [
        BsModalRef,
        CurrentRegistrationService,
        ProfileService,
        AngularDataContext,
        {
          provide: DATA_CONTEXT_CONFIG, useValue: {
              base: '/',
              options: {
                  useMediaTypeExtensions: false
              }
          }
        },
        ConfigurationService,
        {
            provide: APP_INITIALIZER,
            useFactory: (configurationService: ConfigurationService) =>
                () => {
                    configurationService.config = {
                        settings: {
                            localization: {
                                cultures: [ 'en', 'el' ],
                                default: 'en'
                            }
                        }
                    };
                    return Promise.resolve();
                },
            deps: [ ConfigurationService ],
            multi: true
        }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationSpecialtyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
