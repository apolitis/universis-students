import {Component, OnInit} from '@angular/core';
import {ProfileService} from '../../../profile/services/profile.service';
import {Router} from '@angular/router';
import { CurrentRegistrationService } from '../../services/currentRegistrationService.service';

@Component({
    selector: 'app-registrations-home',
    template: `
        <div class="clear-content-area">
            <div class="d-none d-md-block" *ngIf="!isLoading">
                <ul class="nav nav-tabs">
                    <li class="nav-item" *ngIf="useStudentRegisterAction">
                        <a class="nav-link" [routerLink]="['semester']" routerLinkActive="active"
                           [translate]="'Registrations.RegistrationSemester'"></a>
                    </li>
                    <li class="nav-item" *ngIf="!selectSpecialty">
                        <a class="nav-link" [ngClass]="{ active: !useStudentRegisterAction }" [routerLink]="['courses']"
                           routerLinkActive="active"
                           [translate]="'Registrations.RegistrationCourses'"></a>
                    </li>
                    <li class="nav-item" *ngIf="selectSpecialty">
                        <a class="nav-link" [routerLink]="['specialty']" routerLinkActive="active"
                           [translate]="'Registrations.RegistrationSpecialty'"></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" [routerLink]="['list']" routerLinkActive="active"
                           [translate]="'Registrations.RegistrationList'"></a>
                    </li>
                </ul>
            </div>
            <router-outlet></router-outlet>
        </div>
    `
})

export class RegistrationsHomeComponent implements OnInit {
    public useStudentRegisterAction = true;
    public isLoading = true;
    public selectSpecialty;
    constructor(private _profileService: ProfileService,
                private currentReg: CurrentRegistrationService,
                private _router: Router) {
        //
    }

    async ngOnInit() {
        // get student
        const student = await this._profileService.getStudent();
        const effectiveStatus = await this.currentReg.getCurrentRegistrationEffectiveStatus();
        if (student) {
            // set use register action flag
            this.useStudentRegisterAction = student &&
                student.department &&
                student.department.organization &&
                student.department.organization.instituteConfiguration &&
                student.department.organization.instituteConfiguration.useStudentRegisterAction;
        }
        // console.log(effectiveStatus.code);
        // if(effectiveStatus.code == "SELECT_SPECIALTY") {
        //     this.selectSpecialty  = true;
        //     this._router.navigateByUrl('/registrations/specialty');
        // }
        this.isLoading = false;
    }

}
