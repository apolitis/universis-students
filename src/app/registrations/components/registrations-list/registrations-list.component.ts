import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import { ConfigurationService } from 'src/app/shared/services/configuration.service';

@Component({
  selector: 'app-registrations-list',
  templateUrl: './registrations-list.component.html',
  styleUrls: ['./registrations-list.component.scss']
})
export class RegistrationListComponent implements OnInit {

  public loading = true;
  public registrations: any = [];
  public currentLanguage;
  public allRegistrations: any;
  private period: any;
  public periods: any = [];
  private chosenPeriod: any;

  constructor(private context: AngularDataContext, private _configurationService: ConfigurationService) {
    this.currentLanguage = this._configurationService.getCurrentLang();
  }

  ngOnInit() {
      this.context.model('students/me/registrations').asQueryable()
      .expand('classes($orderby=semester,course/displayCode;$expand=course,courseClass($expand=instructors($expand=instructor($select=id,givenName,familyName,category))),courseType)')
          .orderBy('registrationYear desc')
          .thenBy('registrationPeriod desc')
          .getItems().then((res) => {
            this.registrations = this.allRegistrations = res.value;
            this.initializePeriods();
            this.loading = false;
      });
  }

  // Initialize periods for the left menu in order to avoid double period names in the list
  initializePeriods() {
    this.registrations.forEach(registration => {
      // if period is not already in the array, push it
      if (!this.periods.includes(registration.registrationYear.alternateName)){
        this.periods.push(registration.registrationYear.alternateName)
      }
    });
  }

  filterByPeriod(period) {
    this.registrations = this.allRegistrations.filter(x => {
      return x.registrationYear.alternateName === period;
    });
    this.chosenPeriod = period;
  }
}
