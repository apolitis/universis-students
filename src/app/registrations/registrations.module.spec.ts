import {TestBed, async, inject} from '@angular/core/testing';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {RegistrationsModule} from './registrations.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('RegistrationsModule', () => {
  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        RouterTestingModule
      ]
    }).compileComponents();
  }));
  it('should create an instance', inject([TranslateService], (translateService: TranslateService) => {
    const registrationModule = new RegistrationsModule(translateService);
    expect(registrationModule).toBeTruthy();
  }));
});
