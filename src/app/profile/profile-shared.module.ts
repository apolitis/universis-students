import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule, TranslateService} from "@ngx-translate/core";
import {MostModule} from "@themost/angular";
import {environment} from "../../environments/environment";
import { ProfileCardComponent } from './components/profile-card/profile-card.component';
import {SharedModule} from "../shared/shared.module";
import {ProfileService} from "./services/profile.service";

@NgModule({
  imports: [
    CommonModule,
      TranslateModule,
      MostModule,
      SharedModule
  ],
  declarations: [
      ProfileCardComponent
  ],
    exports: [
        ProfileCardComponent
    ],
    providers: [
        ProfileService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProfileSharedModule {

    constructor(private _translateService: TranslateService) {
        environment.languages.forEach((culture) => {
            import(`./i18n/profile.${culture}.json`).then((translations) => {
                this._translateService.setTranslation(culture, translations, true);
            });
        });
    }
}
