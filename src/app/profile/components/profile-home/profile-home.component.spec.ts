import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LocalizedDatePipe } from 'src/app/shared/localized-date.pipe';

import { ProfileHomeComponent } from './profile-home.component';
import { ProfilePreviewComponent } from './../profile-preview/profile-preview.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { UserService } from 'src/app/auth/services/user.service';
import { AngularDataContext, DATA_CONTEXT_CONFIG } from '@themost/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ConfigurationService } from 'src/app/shared/services/configuration.service';
import { APP_INITIALIZER } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { ProfileService } from '../../services/profile.service';
import { BsModalService, ComponentLoaderFactory, PositioningService } from 'ngx-bootstrap';

xdescribe('ProfileHomeComponent', () => {
  let component: ProfileHomeComponent;
  let fixture: ComponentFixture<ProfileHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileHomeComponent, ProfilePreviewComponent ],
      imports: [ HttpClientTestingModule, SharedModule, TranslateModule.forRoot() ],
      providers: [
        BsModalService,
        ProfileService,
        UserService,
        ComponentLoaderFactory,
        PositioningService,
        AngularDataContext,
        {
          provide: DATA_CONTEXT_CONFIG, useValue: {
              base: '/',
              options: {
                  useMediaTypeExtensions: false
              }
          }
        },
        ConfigurationService,
        {
            provide: APP_INITIALIZER,
            useFactory: (configurationService: ConfigurationService) =>
                () => {
                    configurationService.config = {
                        settings: {
                            localization: {
                                cultures: [ 'en', 'el' ],
                                default: 'en'
                            }
                        }
                    };
                    return Promise.resolve();
                },
            deps: [ ConfigurationService ],
            multi: true
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
