import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AngularDataContext, DATA_CONTEXT_CONFIG } from '@themost/angular';
import { ConfigurationService } from 'src/app/shared/services/configuration.service';
import { APP_INITIALIZER } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { GradesService } from './grades.service';
import { GradeScaleService } from '../services/grade-scale.service';

describe('GradesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        GradesService,
        GradeScaleService,
        AngularDataContext,
        {
          provide: DATA_CONTEXT_CONFIG, useValue: {
              base: '/',
              options: {
                  useMediaTypeExtensions: false
              }
          }
        },
        ConfigurationService,
        {
            provide: APP_INITIALIZER,
            useFactory: (configurationService: ConfigurationService) =>
                () => {
                    configurationService.config = {
                        settings: {
                            localization: {
                                cultures: [ 'en', 'el' ],
                                default: 'en'
                            }
                        }
                    };
                    return Promise.resolve();
                },
            deps: [ ConfigurationService ],
            multi: true
        }],
      imports: [HttpClientTestingModule, TranslateModule.forRoot()]
    });
  });

  it('should be created', inject([GradesService], (service: GradesService) => {
    expect(service).toBeTruthy();
  }));
});
