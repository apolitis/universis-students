interface GradesConfiguration {
    groups: Array<GradesGroupConfiguration>;
}

interface GradesGroupConfiguration {
    title: string,
    attribute: string,
    key: string,
    value: string,
}
