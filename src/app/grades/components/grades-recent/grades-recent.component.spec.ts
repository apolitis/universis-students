import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { CollapseModule } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap';
import { SharedModule } from 'src/app/shared/shared.module';
import { GradesService } from '../../services/grades.service';
import { GradeScaleService } from '../../services/grade-scale.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AngularDataContext, DATA_CONTEXT_CONFIG } from '@themost/angular';
import { ConfigurationService } from 'src/app/shared/services/configuration.service';
import { APP_INITIALIZER } from '@angular/core';

import { GradesStatboxComponent } from './../grades-statbox/grades-statbox.component';
import { GradesRecentComponent } from './grades-recent.component';

describe('GradesRecentComponent', () => {
  let component: GradesRecentComponent;
  let fixture: ComponentFixture<GradesRecentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GradesRecentComponent, GradesStatboxComponent ],
      providers: [
        GradesService,
        GradeScaleService,
        AngularDataContext,
        {
          provide: DATA_CONTEXT_CONFIG, useValue: {
              base: '/',
              options: {
                  useMediaTypeExtensions: false
              }
          }
        },
        ConfigurationService,
        {
            provide: APP_INITIALIZER,
            useFactory: (configurationService: ConfigurationService) =>
                () => {
                    configurationService.config = {
                        settings: {
                            localization: {
                                cultures: [ 'en', 'el' ],
                                default: 'en'
                            }
                        }
                    };
                    return Promise.resolve();
                },
            deps: [ ConfigurationService ],
            multi: true
        }
       ],
      imports: [ TranslateModule.forRoot(), CollapseModule, HttpClientTestingModule, ModalModule.forRoot(), SharedModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GradesRecentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
