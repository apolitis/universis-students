import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AngularDataContext, DATA_CONTEXT_CONFIG } from '@themost/angular';
import { ConfigurationService } from 'src/app/shared/services/configuration.service';
import { APP_INITIALIZER } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { GradesService } from '../../services/grades.service';
import { GradeScaleService } from '../../services/grade-scale.service';

import { GradesStatboxComponent } from './grades-statbox.component';

xdescribe('GradesStatboxComponent', () => {
  let component: GradesStatboxComponent;
  let fixture: ComponentFixture<GradesStatboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GradesStatboxComponent ],
      imports: [ HttpClientTestingModule, TranslateModule.forRoot() ],
      providers: [
        GradesService,
        GradeScaleService,
        AngularDataContext,
        {
          provide: DATA_CONTEXT_CONFIG, useValue: {
              base: '/',
              options: {
                  useMediaTypeExtensions: false
              }
          }
        },
        ConfigurationService,
        {
            provide: APP_INITIALIZER,
            useFactory: (configurationService: ConfigurationService) =>
                () => {
                    configurationService.config = {
                        settings: {
                            localization: {
                                cultures: [ 'en', 'el' ],
                                default: 'en'
                            }
                        }
                    };
                    return Promise.resolve();
                },
            deps: [ ConfigurationService ],
            multi: true
        } ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GradesStatboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
