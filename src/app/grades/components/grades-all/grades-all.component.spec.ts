import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GradesService } from '../../services/grades.service';
import { GradeScaleService } from '../../services/grade-scale.service';
import * as GRADES_CONF from '../../config/grades.config.json';
import { TranslateModule } from '@ngx-translate/core';
import { NgPipesModule } from 'ngx-pipes';
import { SharedModule } from 'src/app/shared/shared.module';
import { ModalModule, TooltipModule } from 'ngx-bootstrap';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AngularDataContext, DATA_CONTEXT_CONFIG } from '@themost/angular';
import { ConfigurationService } from 'src/app/shared/services/configuration.service';
import { APP_INITIALIZER } from '@angular/core';

import { GradesAllComponent } from './grades-all.component';
import { GradesStatboxComponent } from './../grades-statbox/grades-statbox.component';

xdescribe('GradesAllComponent', () => {
  let component: GradesAllComponent;
  let fixture: ComponentFixture<GradesAllComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GradesAllComponent, GradesStatboxComponent ],
      providers: [
        GradesService,
        GradeScaleService,
        AngularDataContext,
        {
          provide: DATA_CONTEXT_CONFIG, useValue: {
              base: '/',
              options: {
                  useMediaTypeExtensions: false
              }
          }
        },
        ConfigurationService,
        {
            provide: APP_INITIALIZER,
            useFactory: (configurationService: ConfigurationService) =>
                () => {
                    configurationService.config = {
                        settings: {
                            localization: {
                                cultures: [ 'en', 'el' ],
                                default: 'en'
                            }
                        }
                    };
                    return Promise.resolve();
                },
            deps: [ ConfigurationService ],
            multi: true
        }
      ],
      imports: [
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        NgPipesModule,
        SharedModule,
        ModalModule.forRoot(),
        TooltipModule.forRoot()
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GradesAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
