export const navigation = [
  {
    title: true,
    name: 'Main Menu'
  },
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer',
  },
  {
    name: 'RegistrationsTitle',
    url: '/registrations',
    icon: 'icon-puzzle',
    children: [
      {
        name: 'CoursesRegistrations',
        url: '/registrations/courses',
        icon: 'icon-puzzle'
      },
      {
        name: 'RegistrationsHistory',
        url: '/registrations/list',
        icon: 'icon-puzzle'
      },
    ]
  },

  {
    name: 'GradesTitle',
    url: '/grades',
    icon: 'icon-book-open',
    children: [
      {
        name: 'RecentGrades',
        url: '/grades/recent',
        icon: 'icon-puzzle'
      },
      {
        name: 'AllGrades',
        url: '/grades/all',
        icon: 'icon-puzzle'
      }
    ]
  },

  {
    name: 'RequestsTitle',
    url: '/requests/list',
    icon: 'icon-note',

  },

  {
    divider: true
  },
  {
    title: true,
    name: 'User Menu',
  },
  {
    name: 'ProfileTitle',
    url: '/profile',
    icon: 'icon-user',

  },

  {
    name: 'LanguagesTitle',
    url: '/lang',
    icon: 'icon-globe',
    children: [
      {
        name: 'Ελληνικά',
        url: '/lang/el',
        icon: 'icon-cursor'
      },
      {
        name: 'English',
        url: '/lang/en',
        icon: 'icon-cursor'
      },
    ]

  },
  {
    name: 'Logout',
    url: '/auth/logout',
    icon: 'icon-lock',
    variant: 'notice'
  }
];
