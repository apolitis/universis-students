import {Pipe, PipeTransform} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import * as numeral from 'numeral';
import 'numeral/locales';
import './services/numeral/locales';

@Pipe({
    name: 'semester',
    pure: false
})
export class SemesterPipe implements PipeTransform {

    constructor(private _translateService: TranslateService) {
    }

    transform(value: any, pattern: string = 'long'): any {
        if (typeof value === 'object') {
            value = value.id;
        }
        if (value >= 250) {
            return this._translateService.instant(`Semester.full.${value}`);
        }
        numeral.locale(this._translateService.currentLang);
        return this._translateService.instant(`Semester.${pattern}`,
            {
                value: value,
                ordinal: numeral(value).format('o')
            });
    }
}
