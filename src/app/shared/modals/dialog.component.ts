import { Component, Output, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-dialog',
  template: `
  <div class="modal-header">
    <h4 class="modal-title">{{ title }}</h4>
  </div>
  <div class="modal-body"> {{ description }}</div>
  <div class="modal-footer">
    <button type="button" (click)="confirm()" class="btn btn-primary">OK</button>
    <button type="button" *ngIf="!isAlert" (click)="decline()" class="btn btn-cancel">Cancel</button>
  </div>`
})

export class DialogComponent {
  title: string;
  description: string;
  isAlert: boolean;
  @Output() choice: EventEmitter<boolean> = new EventEmitter();

  constructor(private modalRef: BsModalRef) { }

  confirm() {
    this.choice.emit(true);
    this.modalRef.hide();
  }

  decline() {
    this.choice.emit(false);
    this.modalRef.hide();
  }
}
