import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ConfigurationService } from 'src/app/shared/services/configuration.service';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule}  from '@angular/common/http/testing';
import { APP_INITIALIZER } from '@angular/core';

import { LangswitchComponent } from './langswitch.component';

describe('LangswitchComponent', () => {
  let component: LangswitchComponent;
  let fixture: ComponentFixture<LangswitchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LangswitchComponent ],
      imports: [ TranslateModule.forRoot(), HttpClientTestingModule ],
      providers: [
        ConfigurationService,
        {
            provide: APP_INITIALIZER,
            useFactory: (configurationService: ConfigurationService) =>
                () => {
                    configurationService.config = {
                        settings: {
                            localization: {
                                cultures: [ 'en', 'el' ],
                                default: 'en'
                            }
                        }
                    };
                    return Promise.resolve();
                },
            deps: [ ConfigurationService ],
            multi: true
        }
    ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LangswitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
