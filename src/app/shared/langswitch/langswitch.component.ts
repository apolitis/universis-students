import { Injectable, Component, OnInit } from '@angular/core';
import { ConfigurationService } from '../services/configuration.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-langswitch',
  templateUrl: './langswitch.component.html',
  styleUrls: ['./langswitch.component.scss']
})

export class LangswitchComponent {

  public currentLang: string;
  public languages: string[];
  constructor(private config: ConfigurationService, private translate: TranslateService) { this.initLanguages(); }

  initLanguages() {
    this.currentLang = this.translate.currentLang;
    this.languages = this.config.settings.localization.cultures;
    this.languages = this.languages.filter(item => item !== this.currentLang);
  }

  changeLang(lang) {
    this.config.setCurrentLang(lang);
    this.initLanguages();
  }
}
