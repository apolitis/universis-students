import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TooltipModule } from 'ngx-bootstrap';
import { ProfileService } from 'src/app/profile/services/profile.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AngularDataContext, DATA_CONTEXT_CONFIG } from '@themost/angular';
import { ConfigurationService } from 'src/app/shared/services/configuration.service';
import { APP_INITIALIZER } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { ProgressBarDegreeComponent } from './progress-bar-degree.component';
import { GradesService } from 'src/app/grades/services/grades.service';
import { GradeScaleService } from 'src/app/grades/services/grade-scale.service';

describe('ProgressBarDegreeComponent', () => {
  let component: ProgressBarDegreeComponent;
  let fixture: ComponentFixture<ProgressBarDegreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressBarDegreeComponent ],
      providers: [
        ProfileService,
        GradesService,
        GradeScaleService,
        AngularDataContext,
        {
          provide: DATA_CONTEXT_CONFIG, useValue: {
              base: '/',
              options: {
                  useMediaTypeExtensions: false
              }
          }
        },
        ConfigurationService,
        {
            provide: APP_INITIALIZER,
            useFactory: (configurationService: ConfigurationService) =>
                () => {
                    configurationService.config = {
                        settings: {
                            localization: {
                                cultures: [ 'en', 'el' ],
                                default: 'en'
                            }
                        }
                    };
                    return Promise.resolve();
                },
            deps: [ ConfigurationService ],
            multi: true
        } ],
      imports: [ TooltipModule, HttpClientTestingModule, TranslateModule.forRoot()]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressBarDegreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
