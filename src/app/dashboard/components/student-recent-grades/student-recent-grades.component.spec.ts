import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GradesService } from 'src/app/grades/services/grades.service';
import { GradeScaleService } from 'src/app/grades/services/grade-scale.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AngularDataContext, DATA_CONTEXT_CONFIG } from '@themost/angular';
import { ConfigurationService } from 'src/app/shared/services/configuration.service';
import { APP_INITIALIZER } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { StudentRecentGradesComponent } from './student-recent-grades.component';

describe('StudentRecentGradesComponent', () => {
  let component: StudentRecentGradesComponent;
  let fixture: ComponentFixture<StudentRecentGradesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentRecentGradesComponent ],
      imports: [ HttpClientTestingModule, TranslateModule.forRoot() ],
      providers: [
        GradesService,
        GradeScaleService,
        AngularDataContext,
        {
          provide: DATA_CONTEXT_CONFIG, useValue: {
              base: '/',
              options: {
                  useMediaTypeExtensions: false
              }
          }
        },
        ConfigurationService,
        {
            provide: APP_INITIALIZER,
            useFactory: (configurationService: ConfigurationService) =>
                () => {
                    configurationService.config = {
                        settings: {
                            localization: {
                                cultures: [ 'en', 'el' ],
                                default: 'en'
                            }
                        }
                    };
                    return Promise.resolve();
                },
            deps: [ ConfigurationService ],
            multi: true
        } ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentRecentGradesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
