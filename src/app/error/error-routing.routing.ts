import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorBaseComponent } from "./components/error-base/error-base.component";

const routes: Routes = [
  { path: '', component: ErrorBaseComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ErrorRoutingModule {
}
