import { Component, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { UserService } from '../auth/services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html',
  styleUrls: ['full-layout.component.scss']
  })
export class FullLayoutComponent implements OnInit {

  constructor(private _userService: UserService) {
  }

  public disabled = false;
  public status: { isopen: boolean } = { isopen: false };
  public user: string = '';

  public toggled(open: boolean): void {
    console.log('Dropdown is now: ', open);
  }

  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

  ngOnInit(): void {
    this._userService.getUser().subscribe( user => {
      this.user = user.name;
    });
  }
}
